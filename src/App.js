import React from "react";
import "./App.css";
import { Navbar } from "./app/Components/Layout/Navbar/Navbar";
import { Outlet } from "react-router-dom";
// import { useSelector } from "react-redux";
// import { useNavigate } from "react-router-dom";

function App() {
  // const navigate = useNavigate();
  // const authToken = useSelector((state) => state.auth.istoken);
  // const token = localStorage.getItem("token");

  // useEffect(() => {
  //   if (!token) {
  //     navigate("/");
  //   }
  // }, [token, navigate]);

  return (
    <>
      <Navbar />
      <Outlet />
    </>
  );
}

export default App;
