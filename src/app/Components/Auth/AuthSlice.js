import { createSlice } from "@reduxjs/toolkit";
// import { redirect } from "react-router-dom";

const authSlice = createSlice({
  name: "auth",
  initialState: {
    email: "",
    password: "",
    validated: false,
    // istoken:null
  },
  reducers: {
    login: (state, action) => {
      // localStorage.setItem("token", action.payload.token);
      //  state.istoken=action.payload;
    },
    logout: (state) => {
      // localStorage.removeItem("token");
      // redirect("/");
    },
    setValidate: (state) => {
      state.validated = true;
    },
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    setPassword: (state, action) => {
      state.password = action.payload;
    },
  },
});

export const { login, logout, setValidate, setEmail, setPassword } =
  authSlice.actions;
export default authSlice.reducer;
