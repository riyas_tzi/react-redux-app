// import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
// import axios from "axios";
import "./Login.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import { login, setValidate, setEmail, setPassword } from "./AuthSlice";

const Login = () => {
  const navigate = useNavigate();
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      debugger
      // dispatch(login({ token: "token" }));
      navigate("/ecommerce");
    }

    dispatch(setValidate());
  };
  return (
    <>
      <div className="loginContainer">
        <ToastContainer />

        <Card className="w-50 m-auto position-absolute top-50 start-50 translate-middle card ">
          <Card.Title className="d-flex justify-content-center mt-3">
            Login
          </Card.Title>
          <Card.Body>
            <Form noValidate validated={auth.validated} onSubmit={handleSubmit}>
              <Form.Group controlId="validationCustom01" className="mb-3">
                <Card.Text className="mb-0">
                  <Form.Label> Enter Email</Form.Label>
                </Card.Text>
                <Form.Control
                  required
                  type="email"
                  placeholder="Email Address"
                  onChange={(e) => dispatch(setEmail(e.target.value))}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  Email is required.
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="validationCustom02" className="mb-3">
                <Card.Text className="mb-0">
                  <Form.Label>Enter Password</Form.Label>
                </Card.Text>
                <Form.Control
                  required
                  type="password"
                  placeholder="Password"
                  onChange={(e) => dispatch(setPassword(e.target.value))}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  Password is required.
                </Form.Control.Feedback>
              </Form.Group>
              <div className="mt-3 d-flex justify-content-center">
                <Button type="submit">Submit form</Button>
              </div>
            </Form>
          </Card.Body>
        </Card>
      </div>
    </>
  );
};

export default Login;
