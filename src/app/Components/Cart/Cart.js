import { useDispatch, useSelector } from "react-redux";
import { showCart } from "./CartSlice";
import { Button } from "react-bootstrap";

const Cart=()=>{
    const qty=useSelector((state)=>state.cart.Qty);
    const dispatch=useDispatch();
    const handleClick=()=>{
        dispatch(showCart())
    } 
    return(
        <>
        <Button onClick={handleClick} className="me-3" variant="warning">
            Cart {qty} Items
         </Button>   
        </>
    )
}

export default Cart;
