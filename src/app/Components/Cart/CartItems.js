import { useDispatch, useSelector } from "react-redux";
import Card from "react-bootstrap/Card";
import Stack from "react-bootstrap/Stack";
import { Button } from "react-bootstrap";
import { addItem, removeItem } from "./CartSlice";

const CartItems = () => {
  const cartItems = useSelector((state) => state.cart.cartItems);

  const dispatch = useDispatch();

  const handleClick = (e, item) => {
    debugger;
    const { name } = e.target;
    e.preventDefault();
    if (name === "increment") {
      dispatch(
        addItem({
          id: item.id,
          name: item.name,
          price: item.price,
          totalPrice:item.totalPrice
        })
      );
    } else if (name === "decrement") {
      dispatch(removeItem(item.id));
    }
  };
  return (
    <>
      <Stack gap={3}>
        {cartItems.map((item) => {
          return (
            <Card key={item.id}>
              <Card.Body className="d-flex">
                <Card.Title style={{ width: "20%" }}>{item.name}</Card.Title>
                <Card.Text style={{ width: "20%" }}>$ {item.price}</Card.Text>
                <Card.Text style={{ width: "20%" }}>X {item.Qty}</Card.Text>
                <Card.Text style={{ width: "20%" }}>
                  $ {item.totalPrice}
                </Card.Text>
                <Card.Text style={{ width: "10%" }}>
                  <Button
                    variant="danger"
                    name="decrement"
                    onClick={(e) => handleClick(e, item)}
                  >
                    {" "}
                    -{" "}
                  </Button>{" "}
                </Card.Text>
                <Card.Text style={{ width: "10%" }}>
                  <Button
                    variant="primary"
                    name="increment"
                    onClick={(e) => handleClick(e, item)}
                  >
                    {" "}
                    +{" "}
                  </Button>{" "}
                </Card.Text>
              </Card.Body>
            </Card>
          );
        })}
      </Stack>
    </>
  );
};

export default CartItems;
