import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cartItems: [],
    id: null,
    name: "",
    price: null,
    totalPrice: null,
    Qty: null,
    showCart: false,
  },
  reducers: {
    addItem(state, action) {
      const newItem = action.payload;
      const existItem = state.cartItems.find((item) => item.id === newItem.id);
      if (existItem) {
        existItem.Qty += 1;
        existItem.totalPrice += newItem.price;
      } else {
        state.cartItems.push({
          name: newItem.name,
          price: newItem.price,
          totalPrice: newItem.totalPrice,
          Qty: 1,
          id: newItem.id,
        });
        state.Qty++;
      }
    },
    removeItem(state, action) {
      const id = action.payload;
      const existItem = state.cartItems.find((item) => item.id === id);
      if (existItem.Qty === 1) {
        state.cartItems = state.cartItems.filter((v) => {
          return v.id !== id;
        });
        state.Qty--;
      } else {
        existItem.Qty -= 1;
        existItem.totalPrice -= existItem.price;
      }
    },
    showCart(state) {
      state.showCart = !state.showCart;
    },
  },
});

export const { addItem, removeItem, showCart, decreaseQty, increaseQty } =
  cartSlice.actions;

export default cartSlice.reducer;
