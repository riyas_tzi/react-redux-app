import { useSelector } from "react-redux";
// import { Navbar } from "../Layout/Navbar/Navbar";
import Products from "../Products/Products";
import CartItems from "../Cart/CartItems";
import Cart from "../Cart/Cart";
const Dashboard = () => {
  let total = 0;
  const cartItems = useSelector((state) => state.cart.cartItems);
  cartItems.forEach((v) => {
    total += v.totalPrice;
  });
  const showCart = useSelector((state) => state.cart.showCart);

  return (
    <>
      <div className="d-flex justify-content-end mt-3 me-2"><Cart /> </div> 
      <Products />
      {showCart && <CartItems />}
      {showCart && (
        <h3 className="d-flex justify-content-end me-5 mt-4">Total= {total}</h3>
      )}
    </>
  );
};
export default Dashboard;
