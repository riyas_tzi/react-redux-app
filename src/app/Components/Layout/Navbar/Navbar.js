import React from "react";
import { useDispatch } from "react-redux";
import { logout } from "../../Auth/AuthSlice";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";


export const Navbar = () => {
  const dispatch = useDispatch();
  const navList=[
    {path:"/posts",name:"Posts"}
  ]
  const handleSubmit = () => {
    dispatch(logout());
  };
  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <div className="navbar-brand">React Redux App</div>
        <div className="navbar-nav ms-auto">
          {
            navList.map((v,i)=>{
             return(
              <Link to={v.path} key={i} className="m-auto me-3 text-white text-decoration-none">{v.name} </Link>
             )
            })
          }
          <Button variant={"danger"} className="me-3" onClick={handleSubmit}>
            Logout
          </Button>
        </div>
      </nav>
    </>
  );
};
