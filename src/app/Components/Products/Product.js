import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";


import { useDispatch } from "react-redux";
import { addItem } from "../Cart/CartSlice";
const Product = ({ name, price, total, path, id }) => {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(
      addItem({
        id: id,
        name: name,
        price: price,
        totalPrice: total,
      })
    );
  };
  return (
 
          <Card>
            <Card.Img variant="top" src={path} height="250px" width="200px" />

            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Text>price {price}</Card.Text>
              <Button variant="primary" onClick={handleClick}>
                Add to Cart
              </Button>
            </Card.Body>
          </Card>

  );
};

export default Product;
