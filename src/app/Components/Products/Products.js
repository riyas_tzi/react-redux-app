import Product from "./Product";
import hp from "../../../product image/hp.png";
import acer from "../../../product image/acer.jpg";
import dell from "../../../product image/dell.avif";
import idealpad from "../../../product image/idealpad.avif";
// import Container-fluid from "react-bootstrap/Container";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";

const Products = () => {
  const productsList = [
    {
      id: 0,
      name: "Hp ",
      price: 20000,
      totalPrice: 20000,
      path: hp,
    },
    {
      id: 1,
      name: "Dell ",
      price: 25000,
      totalPrice: 25000,
      path: dell,
    },
    {
      id: 2,
      name: "Acer ",
      price: 24000,
      totalPrice: 24000,
      path: acer,
    },
    {
      id: 3,
      name: "Idealpad ",
      price: 28000,
      totalPrice: 28000,
      path: idealpad,
    },
  ];
  const listItems = productsList.map((product, index) => {
    return (
      <li key={index} style={{ width: "25%" }}>
        <Product
          name={product.name}
          price={product.price}
          total={product.totalPrice}
          path={product.path}
          id={product.id}
        />
      </li>
    );
  });
  return (
    <>
      {/* <Container> */}
      {/* <Row className="justify-content-center">
        <Col md={12}>
          {listItems} */}
          <ul
              style={{
                listStyleType: "none",
                display: "flex",
                columnGap: "24px",
                padding: "24px",
              }}
            >
              {listItems}
            </ul>
        {/* </Col>
      </Row> */}
      {/* </Container> */}
    </>
  );
};

export default Products;
