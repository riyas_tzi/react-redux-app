import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useAddNewPostMutation } from "../../api/apiSlice";
import Form from "react-bootstrap/Form";
import { Button } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

export const AddPostForm = () => {
  const schema = yup
    .object({
      title: yup.string().required(),
      content: yup.string().required(),
    })
    .required();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ mode: "onBlur", resolver: yupResolver(schema) });

  const [addNewPost] = useAddNewPostMutation();
  const users = useSelector((state) => state.users.users);

  const onSubmit = async (data, e) => {
    if (data) {
      try {
        await addNewPost(data).unwrap();
        e.target.reset();
      } catch (err) {
        console.error("Failed to save the post: ", err);
      }
    }
  };
  const usersOptions = users?.map((user, i) => (
    <option key={user?.id} value={user?.id}>
      {user?.name}
    </option>
  ));

  return (
    <section>
      <h2>Add a New Post</h2>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <div className="d-flex justify-content-center">
          <div className="w-50 d-flex flex-column">
            <Form.Group className="mb-3" controlId="formBasicTitle">
              <Form.Label>Post Title</Form.Label>
              <Form.Control
                type="text"
                aria-invalid={errors.title ? "true" : "false"}
                {...register("title", { required: "title is required" })}
              />

              {errors.title?.type === "required" && (
                <p className="text-danger">{errors.title?.message}</p>
              )}
            </Form.Group>

            <Form.Select
              aria-label="Default select example"
              {...register("user")}
            >
              <option>Open this select menu</option>
              {usersOptions}
            </Form.Select>

            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Content</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                aria-invalid={errors.content ? "true" : "false"}
                {...register("content", { required: "content is required" })}
              />
              {errors.content?.type === "required" && (
                <p className="text-danger">{errors.content?.message}</p>
              )}
            </Form.Group>
            <Button type="submit">Save Post</Button>
          </div>
        </div>
      </Form>
    </section>
  );
};
