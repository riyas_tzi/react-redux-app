import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useGetPostQuery, useEditPostMutation } from "../../api/apiSlice";
import Spinner from "../../Components/Spinner";
import Form from "react-bootstrap/Form";
import { Button } from "react-bootstrap";
import { useSelector } from "react-redux";

export const EditPostForm = () => {
  const params = useParams();
  const { postId } = params;
  const [editPost] = useEditPostMutation();
  const { data: post, isSuccess, isFetching } = useGetPostQuery(postId);

  const usersList = useSelector((state) => state.users.users);
  const [title, setTitle] = useState(post?.title);
  const [content, setContent] = useState(post?.content);
  const [user, setUser] = useState(post?.user);

  const navigate = useNavigate();

  const onTitleChanged = (e) => setTitle(e.target.value);
  const onContentChanged = (e) => setContent(e.target.value);
  const onUserChanged = (e) => setUser(e.target.value);

  const usersOptions = usersList?.map((user, i) => (
    <option key={user?.id} value={user?.id}>
      {user?.name}
    </option>
  ));

  const onSavePostClicked = async () => {
    if (title && content && postId) {
      try {
        await editPost({ title, content, user: postId }).unwrap();
        setTitle("");
        setContent("");
        navigate("/posts");
      } catch (err) {
        console.error("Failed to save the post: ", err);
      }
    }
  };

  let contents;
  if (isFetching) {
    contents = <Spinner text="Loading..." />;
  } else if (isSuccess) {
    contents = (
      <Form>
        <div className="d-flex justify-content-center">
          <div className="w-50 d-flex flex-column">
            <Form.Group className="mb-3" controlId="formBasicTitle">
              <Form.Label>Post Title</Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={title}
                onChange={onTitleChanged}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Select
              aria-label="Default select example"
              value={user}
              onChange={onUserChanged}
            >
              <option>Open this select menu</option>
              {usersOptions}
            </Form.Select>

            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Content</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={content}
                onChange={onContentChanged}
              />
            </Form.Group>
            <Button
              type="button"
              onClick={onSavePostClicked}
              // disabled={!canSave}
            >
              Save Post
            </Button>
          </div>
        </div>
      </Form>
    );
  }

  console.log("title && content", title, content);
  return (
    <section>
      <h2>Edit Post</h2>
      {contents}
    </section>
  );
};
