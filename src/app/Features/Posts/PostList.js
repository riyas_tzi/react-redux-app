import { AddPostForm } from "./AddPostForm";
import { Link } from "react-router-dom";
import { ReactionButtons } from "./ReactionButtons";
import React, { useMemo } from "react";
import PostAuthor from "./PostAuthor";
import TimeAgo from "./TimeAgo";
import Spinner from "../../Components/Spinner";
import { useGetPostsQuery } from "../../api/apiSlice";
import classnames from "classnames";
import { Button } from "react-bootstrap";

const PostExcerpt = ({ post }) => {
  return (
    <article className="post-excerpt">
      <h3>{post.title}</h3>
      <div>
        <PostAuthor userId={post.user} />
        <TimeAgo timestamp={post.date} />
      </div>
      <p className="post-content">{post.body}</p>

      <ReactionButtons post={post} />
      <Link to={`/posts/${post.id}`} className="button muted-button">
        View Post
      </Link>
    </article>
  );
};
export const PostsList = () => {
  const {
    data: posts,
    isLoading,
    isSuccess,
    isError,
    refetch,
    isFetching,
    error,
  } = useGetPostsQuery();

  let content;

  const sortedPosts = useMemo(() => {
    const sortedPosts = posts?.slice();

    sortedPosts?.sort((a, b) => b?.date?.localeCompare(a?.date));
    return sortedPosts;
  }, [posts]);

  if (isLoading) {
    content = <Spinner text="Loading..." />;
  } else if (isSuccess) {
    const renderedPosts = sortedPosts.map((post) => (
      <PostExcerpt key={post.id} post={post} />
    ));

    const containerClassname = classnames("posts-container", {
      disabled: isFetching,
    });

    content = <div className={containerClassname}>{renderedPosts}</div>;
  } else if (isError) {
    content = <div>{error.toString()}</div>;
  }

  return (
    <section className="posts-list">
      <AddPostForm />
      <h2>Posts</h2>
      <Button onClick={refetch}>Refetch Posts</Button>
      {content}
    </section>
  );
};
