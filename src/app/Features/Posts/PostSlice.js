import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
// import { client } from "../../api/client";
import axios from "axios";

// const initialState = [
//   {
//     id: "1",
//     title: "First Post!",
//     content: "Hello!",
//     date: sub(new Date(), { minutes: 10 }).toISOString(),
//   },
//   {
//     id: "2",
//     title: "Second Post",
//     content: "More text",
//     date: sub(new Date(), { minutes: 5 }).toISOString(),
//   },
// ];

const initialState = {
  posts: [],
  status: "idle",
  error: null,
};

const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    postAdded: {
      reducer(state, action) {
        state.posts.push(action.payload);
      },
      prepare(title, content, userId) {
        return {
          payload: {
            id: nanoid(),
            date: new Date().toISOString(),
            title,
            content,
            user: userId,
          },
        };
      },
    },
    postUpdated(state, action) {
      const { id, title, content } = action.payload;
      const existingPost = state.posts.find((post) => post.id === id);
      if (existingPost) {
        existingPost.title = title;
        existingPost.content = content;
      }
    },
    reactionAdded(state, action) {
      const { postId, reaction } = action.payload;
      const existingPost = state.posts.find((post) => post.id === postId);
      if (existingPost) {
        existingPost.reactions[reaction]++;
      }
    },
  },
  // extraReducers(builder) {
  //   builder
  //     .addCase(fetchPosts.pending, (state, action) => {
  //       state.status = "loading";
  //     })
  //     .addCase(fetchPosts.fulfilled, (state, action) => {
  //       state.status = "succeeded";
  //       // Add any fetched posts to the array
  //       state.posts = state.posts.concat(action.payload);
  //     })
  //     .addCase(fetchPosts.rejected, (state, action) => {
  //       state.status = "failed";
  //       state.error = action.error.message;
  //     });
  // },
});

export const selectAllPosts = (state) => state.posts.posts;

export const selectPostById = (state, postId) => {
  // debugger;
  state.posts.posts.find((post) => post.id === postId);
};
// export const fetchPosts = createAsyncThunk("posts/fetchPosts", async () => {
//   const response = await axios.get(
//     "https://jsonplaceholder.typicode.com/posts"
//   );
//   return response.data;
// });

export const { postAdded, postUpdated, reactionAdded } = postsSlice.actions;
export default postsSlice.reducer;
