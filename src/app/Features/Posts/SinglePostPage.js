import React from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useGetPostQuery, useDeletePostMutation } from "../../api/apiSlice";
import Spinner from "../../Components/Spinner";
import { Button } from "react-bootstrap";

export const SinglePostPage = () => {
  const params = useParams();
  const { postId } = params;
  const [deletePost] = useDeletePostMutation();
  // console.log("id", postId);
  const handleClick = (e, id) => {
    e.preventDefault();
    console.log(id);
    deletePost(id);
  };

  const { data: post, isFetching, isSuccess } = useGetPostQuery(postId);
  let content;
  if (isFetching) {
    content = <Spinner text="Loading..." />;
  } else if (isSuccess) {
    content = (
      <article className="post">
        <h2>{post?.title}</h2>
        <p className="post-content">{post?.content}</p>
        <Link to={`/editPost/${post?.id}`} className="button">
          Edit Post
        </Link>
        <Button variant="danger" onClick={(e) => handleClick(e, post?.id)}>
          Delete Post{" "}
        </Button>
      </article>
    );
  }
  if (!post) {
    return (
      <section>
        <h2>Post not found!</h2>
      </section>
    );
  }

  return <section>{content}</section>;
};
