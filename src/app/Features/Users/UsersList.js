import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { selectAllUsers } from "./UsersSlilce";

export const UsersList = () => {
  const users = useSelector(selectAllUsers);
  //   const userStatus = "idle";

  //   const dispatch = useDispatch();

  //   useEffect(() => {
  //     if (userStatus === "idle") {
  //       dispatch(fetchUsers());
  //     }
  //   }, [userStatus, dispatch]);

  const renderedUsers = users?.map((user) => (
    <li key={user.id}>
      <Link to={`/users/${user?.id}`}>{user?.name}</Link>
    </li>
  ));

  return (
    <section>
      <h2>Users</h2>

      <ul>{renderedUsers}</ul>
    </section>
  );
};
