import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  users: [
    { id: 0, name: "riyas" },
    { id: 1, name: "ashik" },
    { id: 2, name: "kalai" },
  ],
  status: "idle",
  error: null,
};

const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  // extraReducers(builder) {
  //   builder
  //     .addCase(fetchUsers.pending, (state, action) => {
  //       state.status = "loading";
  //     })
  //     .addCase(fetchUsers.fulfilled, (state, action) => {
  //       // state.status = "succeeded";
  //       // // Add any fetched posts to the array
  //       // state.users = state.users.concat(action.payload);
  //     })
  //     .addCase(fetchUsers.rejected, (state, action) => {
  //       state.status = "failed";
  //       state.error = action.error.message;
  //     });
  // },
});

export const fetchUsers = createAsyncThunk("users/fetchUsers", async () => {
  const response = await axios.get("https://dummyjson.com/users");
  return response.data;
});

export const selectAllUsers = (state) => state.users;
export const selectUserById = (state, userId) =>
  state.users.users.find((user) => user.id === userId);

export default usersSlice.reducer;
