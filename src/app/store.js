import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./Features/Counter/CounterSlice";
import authReducer from "./Components/Auth/AuthSlice";
import cartReducer from "./Components/Cart/CartSlice";
import postReducer from "./Features/Posts/PostSlice";
import userReducer from "./Features/Users/UsersSlilce";
import { apiSlice } from "./api/apiSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    cart: cartReducer,
    counter: counterReducer,
    posts: postReducer,
    users: userReducer,
    [apiSlice.reducerPath]: apiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(apiSlice.middleware),
});

export default store;
