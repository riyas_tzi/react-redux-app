import "./index.css";
import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import store from "./app/store";
import App from "./App";
import Ecommerce from "./app/Components/Dashboard/Dashboard";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Login from "../src/app/Components/Auth/Login";
import { PostsList } from "./app/Features/Posts/PostList";
import { SinglePostPage } from "./app/Features/Posts/SinglePostPage";
import { EditPostForm } from "./app/Features/Posts/EditPostForm";
import { UsersList } from "./app/Features/Users/UsersList";
import { UserPage } from "./app/Features/Users/UserPage";
import { apiSlice } from "./app/api/apiSlice";
// import { AddPostForm } from "./app/Features/Posts/AddPostForm";


const container = document.getElementById("root");
const root = createRoot(container);

const router = createBrowserRouter([
  {
    path: "/",
    element: <Login />,
  },
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/ecommerce",
        element: <Ecommerce />,
      },
      {
        path: "/posts",
        element: <PostsList />,
      },
      {
        path: "/posts/:postId",
        element: <SinglePostPage />,
      },
      {
        path: "/editPost/:postId",
        element: <EditPostForm />,
      },
      {
        path: "/users",
        element: <UsersList />,
      },
      {
        path: "/users/:userId",
        element: <UserPage />,
      },
    ],
  },
]);

root.render(
  <Provider store={store}>
    <RouterProvider router={router}></RouterProvider>
  </Provider>
);
